package AIF.AerialVehicles.UAVs.Hermes;

import AIF.AerialVehicles.AerialAttackVehicle;
import AIF.Missions.AttackMission;
import AIF.Missions.Mission;
import AIF.Missions.MissionTypeException;

public class Kochav extends Hermes implements AerialAttackVehicle {
    int amountOfMissile;
    String missileModel;

    public Kochav(int amountOfMissile, String missileModel, String cameraType, String sensorType, String pilotName, Mission mission, int hoursOfFlightSinceLastRepair, Boolean readyToFly) {
        super(cameraType, sensorType, pilotName, mission, hoursOfFlightSinceLastRepair, readyToFly);
        this.amountOfMissile = amountOfMissile;
        this.missileModel = missileModel;
    }


    @Override
    public String attack() {
        String message = this.getPilotName() + ": " + this.getClass().getSimpleName() +
                " Attacking " + ((AttackMission) this.getMission()).getTarget() +
                " with: " + this.getMissileModel() + "X" + this.getAmountOfMissile();
        System.out.println(message);
        return message;
    }

    @Override
    public void setMission(Mission mission) throws MissionTypeException{
        this.mission = mission;
    }

    public int getAmountOfMissile() {
        return this.amountOfMissile;
    }

    public String getMissileModel() {
        return this.missileModel;
    }
}
