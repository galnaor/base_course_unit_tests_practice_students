package AIF;

import org.junit.*;
import static org.junit.Assert.*;

public class AppTest {
    private static App app;

    @BeforeClass
    public static void before() {
        app = new App();
    }

    @Test
    public void testAppHasAGreeting() {
        assertNotNull(app.getGreeting());
    }

    @Test
    public void testMySum() {
        assertEquals(13, app.mySum(2));
    }
}
