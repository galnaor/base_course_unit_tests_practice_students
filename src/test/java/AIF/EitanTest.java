package AIF;

import AIF.AerialVehicles.FighterJets.F15;
import AIF.AerialVehicles.UAVs.Haron.Eitan;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

public class EitanTest {
    private static AIFUtil aifUtil;
    private static Eitan eitan;

    @BeforeClass
    public static void beforeClass() {
        aifUtil = new AIFUtil();
        eitan = (Eitan) aifUtil.getAerialVehiclesHashMap().get("Etan");
    }

    @Test
    public void setMissionExecutable() throws MissionTypeException {
        eitan.setMission(aifUtil.getAllMissions().get("intelligence"));
    }

    @Test(expected = MissionTypeException.class)
    public void setMissionUnexecutable() throws MissionTypeException {
        eitan.setMission(aifUtil.getAllMissions().get("bda"));
    }
}
