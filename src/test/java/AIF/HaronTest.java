package AIF;

import AIF.AerialVehicles.UAVs.Haron.Shoval;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HaronTest {
    private static AIFUtil aifUtil;
    private static Shoval shoval;

    @BeforeClass
    public static void beforeClass() {
        aifUtil = new AIFUtil();
        shoval = (Shoval) aifUtil.getAerialVehiclesHashMap().get("Shoval");
    }

    @Test
    public void checkHermesEquilibriumGroupsInGroup() {
        aifUtil.setHoursAndCheck(shoval, 20);
        assertEquals(20, shoval.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkHermesEquilibriumGroupsUnderGroup() {
        aifUtil.setHoursAndCheck(shoval, -6);
        assertEquals(0, shoval.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkHermesEquilibriumGroupsAboveGroup() {
        aifUtil.setHoursAndCheck(shoval, 300);
        assertEquals(0, shoval.getHoursOfFlightSinceLastRepair());
    }


    @Test
    public void checkLimitsHermesLowerBoundLeft() {
        aifUtil.setHoursAndCheck(shoval, -1);
        assertEquals(0, shoval.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkLimitsHermesLowerBound() {
        aifUtil.setHoursAndCheck(shoval, 0);
        assertEquals(0, shoval.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkLimitsHermesLowerBoundRight() {
        aifUtil.setHoursAndCheck(shoval, 1);
        assertEquals(1, shoval.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkLimitsHermesUpperBoundLeft() {
        aifUtil.setHoursAndCheck(shoval, 149);
        assertEquals(149, shoval.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkLimitsHermesUpperBound() {
        aifUtil.setHoursAndCheck(shoval, 150);
        assertEquals(0, shoval.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkLimitsHermesUpperBoundRight() {
        aifUtil.setHoursAndCheck(shoval, 151);
        assertEquals(0, shoval.getHoursOfFlightSinceLastRepair());
    }
}
