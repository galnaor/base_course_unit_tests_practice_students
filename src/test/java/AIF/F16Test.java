package AIF;

import AIF.AerialVehicles.FighterJets.F16;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

public class F16Test {
    private static AIFUtil aifUtil;
    private static F16 f16;

    @BeforeClass
    public static void beforeClass() {
        aifUtil = new AIFUtil();
        f16 = (F16) aifUtil.getAerialVehiclesHashMap().get("F16");
    }

    @Test
    public void setMissionExecutable() throws MissionTypeException {
        f16.setMission(aifUtil.getAllMissions().get("attack"));
    }

    @Test(expected = MissionTypeException.class)
    public void setMissionUnexecutable() throws MissionTypeException {
        f16.setMission(aifUtil.getAllMissions().get("intelligence"));
    }
}
