package AIF;

import AIF.AerialVehicles.UAVs.Hermes.Zik;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

public class ZikTest {
    private static AIFUtil aifUtil;
    private static Zik zik;

    @BeforeClass
    public static void beforeClass() {
        aifUtil = new AIFUtil();
        zik = (Zik) aifUtil.getAerialVehiclesHashMap().get("Zik");
    }

    @Test
    public void setMissionExecutable() throws MissionTypeException {
        zik.setMission(aifUtil.getAllMissions().get("intelligence"));
    }

    @Test(expected = MissionTypeException.class)
    public void setMissionUnexecutable() throws MissionTypeException {
        zik.setMission(aifUtil.getAllMissions().get("attack"));
    }
}
