package AIF;

import AIF.AerialVehicles.FighterJets.F15;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

public class F15Test {
    private static AIFUtil aifUtil;
    private static F15 f15;

    @BeforeClass
    public static void beforeClass() {
        aifUtil = new AIFUtil();
        f15 = (F15) aifUtil.getAerialVehiclesHashMap().get("F15");
    }

    @Test
    public void setMissionExecutable() throws MissionTypeException {
        f15.setMission(aifUtil.getAllMissions().get("intelligence"));
    }

    @Test(expected = MissionTypeException.class)
    public void setMissionUnexecutable() throws MissionTypeException {
        f15.setMission(aifUtil.getAllMissions().get("bda"));
    }
}
