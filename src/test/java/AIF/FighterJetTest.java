package AIF;

import AIF.AerialVehicles.FighterJets.F15;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class FighterJetTest {
    private static AIFUtil aifUtil;
    private static F15 f15;

    @BeforeClass
    public static void beforeClass() {
        aifUtil = new AIFUtil();
        f15 = (F15) aifUtil.getAerialVehiclesHashMap().get("F15");
    }

    @Test
    public void checkFighterJetsEquilibriumGroupsInGroup() {
        aifUtil.setHoursAndCheck(f15, 20);
        assertEquals(20, f15.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkFighterJetsEquilibriumGroupsUnderGroup() {
        aifUtil.setHoursAndCheck(f15, -6);
        assertEquals(0, f15.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkFighterJetsEquilibriumGroupsAboveGroup() {
        aifUtil.setHoursAndCheck(f15, 300);
        assertEquals(0, f15.getHoursOfFlightSinceLastRepair());
    }


    @Test
    public void checkLimitsFighterJetsLowerBoundLeft() {
        aifUtil.setHoursAndCheck(f15, -1);
        assertEquals(0, f15.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkLimitsFighterJetsLowerBound() {
        aifUtil.setHoursAndCheck(f15, 0);
        assertEquals(0, f15.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkLimitsFighterJetsLowerBoundRight() {
        aifUtil.setHoursAndCheck(f15, 1);
        assertEquals(1, f15.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkLimitsFighterJetsUpperBoundLeft() {
        aifUtil.setHoursAndCheck(f15, 249);
        assertEquals(249, f15.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkLimitsFighterJetsUpperBound() {
        aifUtil.setHoursAndCheck(f15, 250);
        assertEquals(0, f15.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkLimitsFighterJetsUpperBoundRight() {
        aifUtil.setHoursAndCheck(f15, 251);
        assertEquals(0, f15.getHoursOfFlightSinceLastRepair());
    }
}
