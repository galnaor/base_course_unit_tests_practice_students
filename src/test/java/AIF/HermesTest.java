package AIF;

import AIF.AerialVehicles.UAVs.Hermes.Kochav;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class HermesTest {
    private static AIFUtil aifUtil;
    private static Kochav kochav;

    @BeforeClass
    public static void beforeClass() {
        aifUtil = new AIFUtil();
        kochav = (Kochav) aifUtil.getAerialVehiclesHashMap().get("Kohav");
    }

    @Test
    public void checkHermesEquilibriumGroupsInGroup() {
        aifUtil.setHoursAndCheck(kochav, 20);
        assertEquals(20, kochav.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkHermesEquilibriumGroupsUnderGroup() {
        aifUtil.setHoursAndCheck(kochav, -6);
        assertEquals(0, kochav.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkHermesEquilibriumGroupsAboveGroup() {
        aifUtil.setHoursAndCheck(kochav, 300);
        assertEquals(0, kochav.getHoursOfFlightSinceLastRepair());
    }


    @Test
    public void checkLimitsHermesLowerBoundLeft() {
        aifUtil.setHoursAndCheck(kochav, -1);
        assertEquals(0, kochav.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkLimitsHermesLowerBound() {
        aifUtil.setHoursAndCheck(kochav, 0);
        assertEquals(0, kochav.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkLimitsHermesLowerBoundRight() {
        aifUtil.setHoursAndCheck(kochav, 1);
        assertEquals(1, kochav.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkLimitsHermesUpperBoundLeft() {
        aifUtil.setHoursAndCheck(kochav, 99);
        assertEquals(99, kochav.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkLimitsHermesUpperBound() {
        aifUtil.setHoursAndCheck(kochav, 100);
        assertEquals(0, kochav.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void checkLimitsHermesUpperBoundRight() {
        aifUtil.setHoursAndCheck(kochav, 101);
        assertEquals(0, kochav.getHoursOfFlightSinceLastRepair());
    }
}